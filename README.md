# lithophane

This program generates an STL file from an image
for printing/engraving lithophanes, reliefs, etc.

To run it, just give it a filename as an argument:

`mklithophane.py myfile.jpg`

There are also several optional arguments:

`-o` `--outfile` Specify the name of the output file; if not specified, it will be produced from the input file name with the extension replaced with “stl”.

`-s` `--scale` Set the scale of the output.  If the image is not a small one, you might want to use a scale of less than 1 (such as 0.1) to shrink it.  Similarly, scales greater than 1 will increase the size of the output.  Default scale is 1.

`-x` `--xscale`  
`-y` `--yscale` Set the X and Y scales individually.  If not specified, they default to the scale defined above.

`-t` `--zscale` The total thickness, or amount of relief, of the layer computed from the image.  The image data is scaled to this thickness.  Useful values for lithophanes might be in the range of 3-4, other values might be useful in other situations (engraving, etc.).  Default value is 1.

`-z` `--zheight` The fixed thickness underneath the image-controlled thickness.  Setting it to zero would put holes in the object where the image thickness was zero.  For 3D printing, a useful value is the first layer thickness.  Default value is 1.

`-v` `--invert` Invert the mapping from image data to thickness.  Default is higher values are thicker, but for lithophanes, you probably want the opposite so lighter areas are thinner (pass more light).

`-m` `--mirror` Reflect the X axis.

The program is designed to operate under Python 3, with the `opencv-python` package installed (for image reading).
