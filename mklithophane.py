#!/usr/bin/env python3

import cv2
import math
import os

from argparse import ArgumentParser


def getheight(points, x, y):
    if args.mirror:
        x = (xsize - 1) - x
    value = float(points[xsize * y + x])
    if args.invert:
        value = span - value
    return zheight + zscale * value


def getvertices(vertexlist, indices):
    vertices = []
    for index in indices:
        vertices.append(vertexlist[index])

    return vertices


def subtractvector(v1, v2):
    return (v1[0] - v2[0], v1[1] - v2[1], v1[2] - v2[2])


parser = ArgumentParser(description='convert image to lithophane STL')

parser.add_argument('infile', nargs=1)
parser.add_argument('-o', '--outfile', default = None)
parser.add_argument('-x', '--xscale', default = None)
parser.add_argument('-y', '--yscale', default = None)
parser.add_argument('-t', '--zscale', default = '1')
parser.add_argument('-s', '--scale', default = '1')
parser.add_argument('-z', '--zheight', default = '1')
parser.add_argument('-i', '--invert', action = 'store_true')
parser.add_argument('-m', '--mirror', action = 'store_true')
parser.add_argument('-a', '--ascii', action = 'store_true')
parser.add_argument('-v', '--verbose', action = 'store_true')
parser.add_argument('-d', '--debug', action = 'store_true')

args = parser.parse_args()

zheight = float(args.zheight)
xscale = float(args.xscale) if args.xscale else float(args.scale)
yscale = float(args.yscale) if args.yscale else float(args.scale)

inpath = args.infile[0]
img = cv2.imread(inpath)
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ysize, xsize = gray.shape
points = gray.tobytes()

if args.debug:
    for point in points:
        print(float(point))

span = gray.max() - gray.min()
if span == 0:
    span = 1
zscale = float(args.zscale) / span

name = os.path.splitext(inpath)[0]

if args.outfile is None:
    args.outfile = name + '.stl'

facets = []

# make top

for x in range(xsize - 1):
    for y in range(ysize - 1):
        vertices = []
        for xd in range(2):
            for yd in range(2):
                xp = x + xd
                yp = y + yd
                h = getheight(points, xp, yp)
                vertices.append((xp * xscale, yp * yscale, h))
        if args.debug:
            print("top {}".format(vertices))
        # upper left triangle
        facets.append(getvertices(vertices, [0, 3, 1]))
        # lower right triangle
        facets.append(getvertices(vertices, [0, 2, 3]))

# make near side

for x in range(xsize - 1):
    vertices = []
    for xd in range(2):
        xp = x + xd
        yp = 0
        for zp in [0.0, getheight(points, xp, yp)]:
            vertices.append((xp * xscale, yp * yscale, zp))
    if args.debug:
        print("near {}".format(vertices))
    facets.append(getvertices(vertices, [0, 3, 1]))
    facets.append(getvertices(vertices, [0, 2, 3]))

# make far side
        
for x in range(xsize - 1):
    vertices = []
    for xd in range(2):
        xp = x + xd
        yp = ysize - 1
        for zp in [0.0, getheight(points, xp, yp)]:
            vertices.append((xp * xscale, yp * yscale, zp))
    if args.debug:
        print("far {}".format(vertices))
    facets.append(getvertices(vertices, [0, 1, 3]))
    facets.append(getvertices(vertices, [0, 3, 2]))

# make left side

for y in range(ysize - 1):
    vertices = []
    for yd in range(2):
        xp = 0
        yp = y + yd
        for zp in [0.0, getheight(points, xp, yp)]:
            vertices.append((xp * xscale, yp * yscale, zp))
    if args.debug:
        print("left {}".format(vertices))
    facets.append(getvertices(vertices, [0, 1, 3]))
    facets.append(getvertices(vertices, [0, 3, 2]))

# make right side

for y in range(ysize - 1):
    vertices = []
    for yd in range(2):
        xp = xsize - 1
        yp = y + yd
        for zp in [0.0, getheight(points, xp, yp)]:
            vertices.append((xp * xscale, yp * yscale, zp))
    if args.debug:
        print("right {}".format(vertices))
    facets.append(getvertices(vertices, [0, 3, 1]))
    facets.append(getvertices(vertices, [0, 2, 3]))

# make bottom

vertices = []
for xp in [0, xsize - 1]:
    for yp in [0, ysize - 1]:
        vertices.append((xp * xscale, yp * yscale, 0.0))
if args.debug:
    print("bottom {}".format(vertices))
#facets.append(getvertices(vertices, [0, 2, 1]))
facets.append(getvertices(vertices, [0, 1, 2]))
#facets.append(getvertices(vertices, [3, 1, 2]))
facets.append(getvertices(vertices, [3, 2, 1]))

# write STL file
outfile = open(args.outfile, 'w')

outfile.write('solid {}\n'.format(name))

# add facets to STL object

for facet in facets:
    # calculate normal
    u = subtractvector(facet[1], facet[0])
    v = subtractvector(facet[2], facet[0])

    n = (
        u[1] * v[2] - u[2] * v[1],
        u[2] * v[0] - u[0] * v[2],
        u[0] * v[1] - u[1] * v[0])

    magnitude = math.sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2])

    if magnitude == 0:
        normal = n
    else:
        normal = [x / magnitude for x in n]

    outfile.write('  facet normal {} {} {}\n'.format(
        normal[0], normal[1], normal[2]))

    outfile.write('    outer loop\n')
    
    for vertex in facet:
        outfile.write('      vertex {} {} {}\n'.format(
            vertex[0], vertex[1], vertex[2]))

    outfile.write('    endloop\n')
    outfile.write('  endfacet\n')

outfile.write('endsolid {}'.format(name))
